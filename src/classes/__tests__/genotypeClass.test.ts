import { Genotype } from "../genotypeClass";

describe("genotype class", () => {
  it("does string representation", () => {
    let geno = new Genotype("ee", "AA");
    expect(geno.stringRepresentation()).toEqual("eeAAnn");
  });
});
