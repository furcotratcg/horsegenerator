export class Genotype {
  mc1r: string;
  asip: string;
  champagne: string;

  constructor(mc1r: string = "EE", asip: string = "AA", champagne = "nn") {
    this.mc1r = mc1r;
    this.asip = asip;
    this.champagne = champagne;
  }

  stringRepresentation() {
    return this.mc1r + this.asip + this.champagne;
  }
}
