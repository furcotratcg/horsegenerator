import Color from "color";
import { generateCoatColor } from "../coatColorGenerator";
import { Genotype } from "../../classes/genotypeClass";
import { COLOR_DICT, getRandomColorInRange } from "../colors";

let dataProvider = [
  {
    genotype: { mc1r: "EE", asip: "aa" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.black.value),
      bodyColor: Color.hsl(...COLOR_DICT.black.value),
      hairColor: Color.hsl(...COLOR_DICT.black.value),
    },
  },
  {
    genotype: { mc1r: "Ee", asip: "aa" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.black.value),
      bodyColor: Color.hsl(...COLOR_DICT.black.value),
      hairColor: Color.hsl(...COLOR_DICT.black.value),
    },
  },
  {
    genotype: { mc1r: "ee", asip: "aa" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.chestnut.value),
      bodyColor: Color.hsl(...COLOR_DICT.chestnut.value),
      hairColor: Color.hsl(...COLOR_DICT.chestnut.value),
    },
  },
  {
    genotype: { mc1r: "EE", asip: "AA" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.black.value),
      bodyColor: Color.hsl(...COLOR_DICT.brown.value),
      hairColor: Color.hsl(...COLOR_DICT.black.value),
    },
  },
  {
    genotype: { mc1r: "ee", asip: "AA" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.chestnut.value),
      bodyColor: Color.hsl(...COLOR_DICT.chestnut.value),
      hairColor: Color.hsl(...COLOR_DICT.chestnut.value),
    },
  },
  {
    genotype: { mc1r: "EE", asip: "Aa" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.black.value),
      bodyColor: Color.hsl(...COLOR_DICT.brown.value),
      hairColor: Color.hsl(...COLOR_DICT.black.value),
    },
  },

  {
    genotype: { mc1r: "EE", asip: "aa", champagne: "nC" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.classic_champagne.value),
      bodyColor: Color.hsl(...COLOR_DICT.classic_champagne.value),
      hairColor: Color.hsl(...COLOR_DICT.classic_champagne_hair.value),
    },
  },
  {
    genotype: { mc1r: "EE", asip: "Aa", champagne: "nC" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.amber_champagne_point.value),
      bodyColor: Color.hsl(...COLOR_DICT.amber_champagne.value),
      hairColor: Color.hsl(...COLOR_DICT.amber_champagne_point.value),
    },
  },
  {
    genotype: { mc1r: "ee", asip: "aa", champagne: "nC" },
    expectedPhenotype: {
      pointColor: Color.hsl(...COLOR_DICT.gold_champagne.value),
      bodyColor: Color.hsl(...COLOR_DICT.gold_champagne.value),
      hairColor: Color.hsl(...COLOR_DICT.gold_champagne_hair.value),
    },
  },
];

describe.each(dataProvider)("Genotype", (testData) => {
  it(`${testData.genotype} should have expected phenotype`, () => {
    let actual = generateCoatColor(
      new Genotype(
        testData.genotype.mc1r,
        testData.genotype.asip,
        testData.genotype.champagne
      ),
      false // Use non-randomized values for testing purposes
    );
    expect(actual).toEqual(testData.expectedPhenotype);
  });
});

describe("randomColorInRange", () => {
  it("properly throws exception", () => {
    try {
      getRandomColorInRange([10, 10, 10], [5, 50, 3]);
    } catch (error) {
      expect(error).toHaveProperty(
        "message",
        "Invalid range for saturation: 10+-50"
      );
    }
  });
});
