import Color from "color";
import { Genotype } from "src/classes/genotypeClass";
import { ColorGen } from "./colors";

export interface IPhenoString {
  pointColor: string;
  bodyColor: string;
  hairColor: string;
}

export interface IPheno {
  pointColor: Color;
  bodyColor: Color;
  hairColor: Color;
}

export const phenoMap = {
  black: {
    pointColor: "black",
    bodyColor: "black",
    hairColor: "black",
  },
  chestnut: {
    pointColor: "chestnut",
    bodyColor: "chestnut",
    hairColor: "chestnut",
  },
  bay: {
    pointColor: "black",
    bodyColor: "brown",
    hairColor: "black",
  },
  classic_champagne: {
    pointColor: "classic_champagne",
    bodyColor: "classic_champagne",
    hairColor: "classic_champagne_hair",
  },
  amber_champagne: {
    pointColor: "amber_champagne_point",
    bodyColor: "amber_champagne",
    hairColor: "amber_champagne_point",
  },
  gold_champagne: {
    pointColor: "gold_champagne",
    bodyColor: "gold_champagne",
    hairColor: "gold_champagne_hair",
  },
};

function phenoStringToPhenoColor(
  phenoMap: IPhenoString,
  randomized: boolean
): IPheno {
  let colorGen = new ColorGen();
  return {
    pointColor: colorGen.generateColor(phenoMap.pointColor, randomized),
    bodyColor: colorGen.generateColor(phenoMap.bodyColor, randomized),
    hairColor: colorGen.generateColor(phenoMap.hairColor, randomized),
  };
}

export function generateCoatColor(
  genotype: Genotype,
  randomized: boolean = true
): IPheno {
  let pheno = "chestnut";

  if (genotype.mc1r.includes("E")) {
    pheno = "black";

    if (genotype.champagne.includes("C")) {
      pheno = "classic_champagne";
    }

    if (genotype.asip.includes("A")) {
      pheno = "bay";
      if (genotype.champagne.includes("C")) {
        pheno = "amber_champagne";
      }
    }
  } else {
    if (genotype.champagne.includes("C")) {
      pheno = "gold_champagne";
    }
  }

  return phenoStringToPhenoColor(phenoMap[pheno], randomized);
}
