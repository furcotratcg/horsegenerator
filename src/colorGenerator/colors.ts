import Color from "color";
import { getRandomInt } from "../../src/utils/random";

// color values in HSL
export const COLOR_DICT = {
  black: {
    value: [8, 15, 10],
    range: [6, 10, 1],
  },
  chestnut: {
    value: [20, 58, 37],
    range: [4, 7, 3],
  },
  brown: {
    value: [20, 42, 22],
    range: [4, 10, 3],
  },
  classic_champagne: {
    value: [39, 19, 53],
    range: [4, 2, 3],
  },
  classic_champagne_hair: {
    value: [15, 30, 17],
    range: [4, 5, 3],
  },
  amber_champagne: {
    value: [36, 39, 70],
    range: [4, 4, 5],
  },
  amber_champagne_point: {
    value: [22, 30, 33],
    range: [10, 3, 3],
  },
  gold_champagne: {
    value: [31, 40, 71],
    range: [3, 3, 3],
  },
  gold_champagne_hair: {
    value: [33, 25, 85],
    range: [3, 3, 8],
  },
};

export function getRandomColorInRange(value: number[], range: number[]) {
  for (let i = 0; i < 3; i++) {
    if (value[i] + range[i] > 100 || value[i] - range[i] < 0) {
      throw new Error(
        "Invalid range for " +
          ["hue", "saturation", "lightness"][i] +
          ": " +
          value[i] +
          "+-" +
          range[i]
      );
    }
  }

  let hue = getRandomInt(value[0] - range[0], value[0] + range[0]);
  let saturation = getRandomInt(value[1] - range[1], value[1] + range[1]);
  let lightness = getRandomInt(value[2] - range[2], value[2] + range[2]);

  return Color.hsl(hue, saturation, lightness);
}

function getColor(value: number[], range: number[], randomized: boolean) {
  if (!randomized) {
    return Color.hsl(value);
  }
  return getRandomColorInRange(value, range);
}

export class ColorGen {
  colors: Object;
  constructor() {
    this.colors = {};
  }
  generateColor(colorName: string, randomized: boolean) {
    if (this.colors[colorName] === undefined) {
      this.colors[colorName] = getColor(
        COLOR_DICT[colorName].value,
        COLOR_DICT[colorName].range,
        randomized
      );
    }
    return this.colors[colorName];
  }
}
