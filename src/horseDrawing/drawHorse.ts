import Color = require("color");
import { Genotype } from "../../src/classes/genotypeClass";
import { IPheno } from "../../src/colorGenerator/coatColorGenerator";
import { makeText } from "../../src/utils/canvasUtils";
import { getRandomInt } from "../../src/utils/random";
import BitmapData from "../../node_modules/openfl/lib/openfl/display/BitmapData";
import Bitmap from "../../node_modules/openfl/lib/openfl/display/Bitmap";
import Rectangle from "../../node_modules/openfl/lib/openfl/geom/Rectangle";
import Point from "../../node_modules/openfl/lib/openfl/geom/Point";
import BitmapDataChannel from "../../node_modules/openfl/lib/openfl/display/BitmapDataChannel";
import { rgbToRgba } from "../../src/utils/utils";
import { drawBodyAndPoints } from "./drawHorseBody";

const SIZE = 100;
const PADDING = 10;

const HORSE_WIDTH = 454;
const HORSE_HEIGHT = 315;
const SCALE = 1;

function loadFromFilePromise(filename: string): Promise<BitmapData> {
  return new Promise(function (resolve, reject) {
    try {
      return BitmapData.loadFromFile(filename).onComplete(resolve);
    } catch {
      reject();
    }
  });
}

function drawHorseElement(bitmapData: BitmapData, app: any) {
  var bitmap = new Bitmap(bitmapData);
  bitmap.width = HORSE_WIDTH * SCALE;
  bitmap.height = HORSE_HEIGHT * SCALE;
  bitmap.x = 0;
  bitmap.y = 0;
  app.addChild(bitmap);
}

function drawMaskedElement(color: Color, maskBitmapData: BitmapData, app: any) {
  let squareBitmapData = new BitmapData(
    HORSE_WIDTH * SCALE,
    HORSE_HEIGHT * SCALE,
    true,
    rgbToRgba(color.rgbNumber())
  );
  var rect: Rectangle = new Rectangle(
    0,
    0,
    maskBitmapData.width,
    maskBitmapData.height
  );
  squareBitmapData.copyChannel(
    maskBitmapData,
    rect,
    new Point(),
    BitmapDataChannel.ALPHA,
    BitmapDataChannel.ALPHA
  );
  let bitmap = new Bitmap(squareBitmapData);
  app.addChild(bitmap);
}

/**
 * This procedure calls the functions to assemble the horse on screen
 * @param genotype
 * @param pheno
 * @param app
 */
export async function drawHorse(genotype: Genotype, pheno: IPheno, app: any) {
  let horseBodyBitmapData = await loadFromFilePromise(
    "horseAssets/horseBodyMask.png"
  );
  // Body
  drawBodyAndPoints(pheno, horseBodyBitmapData, app);

  // Non-body elements
  // Eyes
  let eyeMaskBitmapData = await loadFromFilePromise("horseAssets/eyeMask.png");
  drawMaskedElement(Color.rgb(0, 0, 0), eyeMaskBitmapData, app);

  // Hooves
  let hoofMaskBitmapData = await loadFromFilePromise(
    "horseAssets/hoofMask.png"
  );
  drawMaskedElement(Color.rgb(75, 75, 75), hoofMaskBitmapData, app);

  // Mane
  let maneMaskBitmapData = await loadFromFilePromise(
    "horseAssets/maneMask.png"
  );
  drawMaskedElement(pheno.hairColor, maneMaskBitmapData, app);

  // Tail
  let tailMaskBitmapData = await loadFromFilePromise(
    "horseAssets/tailMask.png"
  );
  drawMaskedElement(pheno.hairColor, tailMaskBitmapData, app);

  let lineartBitmapData = await loadFromFilePromise(
    "horseAssets/horseLines.png"
  );
  drawHorseElement(lineartBitmapData, app);
  app.addChild(makeText(genotype.stringRepresentation(), 0, 0));
}
