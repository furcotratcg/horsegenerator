// This file handles adding markings and such to the horse's body
import Color = require("color");
import { Genotype } from "../../src/classes/genotypeClass";
import { IPheno } from "../../src/colorGenerator/coatColorGenerator";
import { getRandomInt } from "../../src/utils/random";
import BitmapData from "../../node_modules/openfl/lib/openfl/display/BitmapData";
import Bitmap from "../../node_modules/openfl/lib/openfl/display/Bitmap";
import Rectangle from "../../node_modules/openfl/lib/openfl/geom/Rectangle";
import Point from "../../node_modules/openfl/lib/openfl/geom/Point";
import BitmapDataChannel from "../../node_modules/openfl/lib/openfl/display/BitmapDataChannel";
import BlurFilter from "../../node_modules/openfl/lib/openfl/filters/BlurFilter";
import BitmapFilterQuality from "../../node_modules/openfl/lib/openfl/filters/BitmapFilterQuality";
import { rgbToRgba } from "../../src/utils/utils";

const SIZE = 100;
const PADDING = 10;

const HORSE_WIDTH = 454;
const HORSE_HEIGHT = 315;
const SCALE = 1;

function addPoints(horseColorBitmapData: BitmapData, pointColor: Color) {
  // add points
  let point1Rect = new Rectangle(85, 224, 60, 100);
  let point2Rect = new Rectangle(170, 239, 60, 100);
  let point3Rect = new Rectangle(238, 216, 70, 100);
  let point4Rect = new Rectangle(317, 221, 57, 180);

  horseColorBitmapData.fillRect(point1Rect, rgbToRgba(pointColor.rgbNumber()));
  horseColorBitmapData.fillRect(point2Rect, rgbToRgba(pointColor.rgbNumber()));
  horseColorBitmapData.fillRect(point3Rect, rgbToRgba(pointColor.rgbNumber()));
  horseColorBitmapData.fillRect(point4Rect, rgbToRgba(pointColor.rgbNumber()));

  // Blur the edges
  let blurFilter = new BlurFilter(0, 20, BitmapFilterQuality.MEDIUM);
  var wholeScreenRect: Rectangle = new Rectangle(
    0,
    0,
    horseColorBitmapData.width,
    horseColorBitmapData.height
  );

  horseColorBitmapData.applyFilter(
    horseColorBitmapData,
    wholeScreenRect,
    new Point(0, 0),
    blurFilter
  );
}

export function drawBodyAndPoints(
  pheno: IPheno,
  maskBitmapData: BitmapData,
  app: any
) {
  let bodyColor = pheno.bodyColor;
  let pointColor = pheno.pointColor;

  // This bitmap covers the whole canvas and is where we will render our patterns
  // Fill the whole canvas with the base body color
  let horseColorBitmapData = new BitmapData(
    HORSE_WIDTH,
    HORSE_HEIGHT,
    true,
    rgbToRgba(bodyColor.rgbNumber())
  );

  // Add points
  addPoints(horseColorBitmapData, pointColor);

  // Here we copy alpha transparency values from the mask to our bodycolor/pattern bitmap
  var wholeScreenRect: Rectangle = new Rectangle(
    0,
    0,
    maskBitmapData.width,
    maskBitmapData.height
  );
  horseColorBitmapData.copyChannel(
    maskBitmapData,
    wholeScreenRect,
    new Point(),
    BitmapDataChannel.ALPHA,
    BitmapDataChannel.ALPHA
  );

  let maskedSquareBitmap = new Bitmap(horseColorBitmapData);
  app.addChild(maskedSquareBitmap);
}
