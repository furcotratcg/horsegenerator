import Color = require("color");
import { Genotype } from "../../src/classes/genotypeClass";
import {
  generateCoatColor,
  IPheno,
} from "../../src/colorGenerator/coatColorGenerator";
import { getRandomInt } from "../../src/utils/random";
import { drawHorse } from "../../src/horseDrawing/drawHorse";

const SIZE = 100;
const PADDING = 10;

const HORSE_WIDTH = 454;
const HORSE_HEIGHT = 315;
const SCALE = 1;

function generateRandomHorse(): [Genotype, IPheno] {
  let mc1rGeneOptions = ["ee", "Ee", "EE", "eE"];
  // let mc1rGeneOptions = ["ee"];
  let mc1rgenotype =
    mc1rGeneOptions[getRandomInt(0, mc1rGeneOptions.length - 1)];

  let asipGeneOptions = ["aa", "Aa", "AA", "aA"];
  // let asipGeneOptions = ["aa"];
  let asipGenotype =
    asipGeneOptions[getRandomInt(0, asipGeneOptions.length - 1)];

  // let champagneGeneOptions = ["nn", "Cn", "CC", "nC"];
  let champagneGeneOptions = ["Cn"];
  let champagneGenotype =
    champagneGeneOptions[getRandomInt(0, champagneGeneOptions.length - 1)];
  let genotype = new Genotype(mc1rgenotype, asipGenotype, champagneGenotype);
  let coatColor = generateCoatColor(genotype, true);
  return [genotype, coatColor];
}

export async function generateRandomHorses(app: any) {
  let [genotype, pheno] = generateRandomHorse();
  await drawHorse(genotype, pheno, app);
}
