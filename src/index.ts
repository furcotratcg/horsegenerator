import Sprite from "openfl/display/Sprite";
import Stage from "openfl/display/Stage";
import Loader from "../node_modules/openfl/lib/openfl/display/Loader";
import URLRequest from "../node_modules/openfl/lib/openfl/net/URLRequest";
import Event from "openfl/events/Event";

import TextField from "openfl/text/TextField";

import { generateRandomHorses } from "./horseGenerator/generateRandomHorse";
import BitmapData from "../node_modules/openfl/lib/openfl/display/BitmapData";
import Bitmap from "../node_modules/openfl/lib/openfl/display/Bitmap";
import Color from "color";
import color from "color";

class App extends Sprite {
  constructor() {
    super();
    generateRandomHorses(this);
    // [15, 25, 10] ->hsl(38, 31, 35) ->
    // hsl([17, 55, 35]) ->hsl(31, 25%, 59%)
    // [17, 52, 20 -> hsl(36, 34%, 64%)
    // Transformation: shift hue to green (30-38)
    // multiply sat by *.66, floor saturation at 25
    // multiply lightness to 3
    this.graphics.beginFill(color.hsl(17, 52, 20).rgbNumber());
    this.graphics.drawRect(300, 300, 100, 100);
    this.graphics.beginFill(color.hsl(36, 34, 64).rgbNumber());
    this.graphics.drawRect(400, 300, 100, 100);
  }
}

var stage = new Stage(550, 400, 0xffffff, App);
document.body.appendChild(stage.element);
