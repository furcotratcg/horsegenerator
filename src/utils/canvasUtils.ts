import TextField from "../../node_modules/openfl/lib/openfl/text/TextField";

export function makeText(text: string, x: number, y: number) {
  var textField = new TextField();

  textField.selectable = false;

  textField.x = x;
  textField.y = y;
  textField.text = text;

  return textField;
}
