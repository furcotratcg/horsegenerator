export function rgbToRgba(rgbNumber: number): number {
  return -1 * (16777216 - rgbNumber);
}
