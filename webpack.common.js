const path = require ('path');

module.exports = {
	// entry: "./src/app.ts",
	output: {
		path: path.resolve (__dirname, "dist"),
		filename: "app.js"
	},
	resolve: {
		alias: {
			"openfl": path.resolve (__dirname, "node_modules/openfl/lib/openfl")
		},
		extensions: [
			'.ts',
			'.tsx',
			'.js'
		],
		modules: ['node_modules', path.resolve(__dirname, 'core')]
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: 'ts-loader'
			}
		]
	}
};